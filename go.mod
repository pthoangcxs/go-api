module go-api

go 1.13

require (
	github.com/badoux/checkmail v0.0.0-20181210160741-9661bd69e9ad
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.7.3
	github.com/jedrp/go-core v0.0.24-beta
	github.com/jinzhu/gorm v1.9.11
	github.com/joho/godotenv v1.3.0
	golang.org/x/crypto v0.0.0-20191206172530-e9b2fee46413
	gopkg.in/square/go-jose.v2 v2.4.0
)
