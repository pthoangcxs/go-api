package middlewares

import (
	"errors"
	"fmt"
	"go-api/app/responses"
	"net/http"
	"strings"

	"github.com/jedrp/go-core/jwt"
)

var mySigningKey = []byte("captainjacksparrowsayshi")

func SetMiddlewareJSON(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next(w, r)
	}
}

func SetMiddlewareAuthentication(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := TokenValid(r)
		if err != nil {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
			return
		}
		next(w, r)
	}
}
func ValidateTokenMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		authorizationHeader := req.Header.Get("Authorization")
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				initConfig := &jwt.JwtValidatorConfig{
					Issuer:     "http://localhost:5000",
					Aud:        "go-server-pthoang",
					JwkAddress: "http://localhost:5000/.well-known/openid-configuration/jwks",
				}
				gẹtwks := func() jwt.Jwks {
					jwks, err := initConfig.GetJwks()
					if err != nil {
						fmt.Println(err)
					}
					return *jwks
				}
				config := jwt.NewJwtValidatorConfig(initConfig.Aud, initConfig.Issuer, gẹtwks)
				token, error := config.ValidateToken(bearerToken[1])
				if error != nil {
					responses.ERROR(w, http.StatusUnauthorized, errors.New("invalid token"))
					return
				}
				if token.Valid {

					next(w, req)
				} else {
					responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
				}
			} else {
				responses.ERROR(w, http.StatusUnauthorized, errors.New("not authenticated"))
			}
		} else {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("end authen"))
		}
	})
}
