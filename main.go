package main

// Importing packages
import (
	"go-api/app"
)

// Main function
func main() {

	app.Run()

}
